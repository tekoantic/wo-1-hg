package ch.marjan.hausaufgaben1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebViewActivity extends AppCompatActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        webView = (WebView) findViewById(R.id.webViewActivity_webView);
        Bundle bundle = getIntent().getExtras();

        webView.loadData(bundle.getString(InputActivity.KEY_1), "text/html; charset=utf-8", "utf-8");

    }
}
